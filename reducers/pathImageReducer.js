import { URL_IMAGE } from '../actionsConstants';

let initState = '';

const pathImageReducer = (state = initState, action) => {
    switch (action.type) {
        case URL_IMAGE:
            return action.payload.path;
        default: 
            return state;
    }
}

export default pathImageReducer;