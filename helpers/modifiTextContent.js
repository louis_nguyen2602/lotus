const modifiTextContent = (listTags, id, newText) => {
    if(!listTags) {
        return new Array();
    }
     if(listTags.findIndex(tag => tag.id === id) > -1) {
        return listTags.map(tag => {
            if(tag.id === id) {
                return {...tag, text: newText}
            } else {
                return {...tag};
            }
        })
    } else {
        return listTags.map(tag => {
            return {...tag, children: modifiTextContent(tag.children, id, newText)};
        })
    }
}

export default modifiTextContent;