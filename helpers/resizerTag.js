export let bottomResizer =  {
    id: "bottom-resizer-outside",
    name: "bottom-resizer",
    type: 'div',
    text: '',
    className: '',
    style: {
        width: 15,
        height: 15,
        position: 'absolute',
        zIndex: 99,
        bottom: 0,
        left: 0,
        right: 0,
        marginTop: 0,
        marginBottom: 0,
        marginLeft: 'auto',
        marginRight: 'auto',
        cursor: 'ns-resize',
    },
    children: [
        {
            id: "bottom-resizer-inside",
            name: "bottom-resizer",
            type: 'div',
            text: '',
            className: '',
            style: {
                width: 15,
                height: 2,
                position: 'absolute',
                bottom: 0,
                margin: 'auto',
                backgroundColor: 'red'
            },
            children: []
        }
    ]
}
export let rightResizer =  {
    id: "right-resizer-outside",
    name: "right-resizer",
    type: 'div',
    text: '',
    className: '',
    style: {
        width: 15,
        height: 15,
        position: 'absolute',
        zIndex: 99,
        bottom: 0,
        top: 0,
        bottom: 0,
        right: 0,
        marginTop: 'auto',
        marginBottom: 'auto',
        marginLeft: 0,
        marginRight: 0,
        cursor: 'ew-resize',
    },
    children: [
        {
            id: "right-resizer-inside",
            name: "right-resizer",
            type: 'div',
            text: '',
            className: '',
            style: {
                width: 2,
                height: 15,
                position: 'absolute',
                right: 0,
                margin: 'auto',
                backgroundColor: 'red'
            },
            children: []
        }
    ]
}