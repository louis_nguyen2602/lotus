const addTag = (listTags, id, newTag) => {
    return listTags.map(tag => {
        if(tag.id === id) {
            return {...tag, children: [...tag.children, newTag]};
        } else {
            return {...tag, children: addTag(tag.children, id, newTag)};
        }
    })
}

export default addTag;