const getCSSProperty = (property, DOM) => {
    if(!DOM) {
        return '';
    }
    let numberProperty = ['width', 'height','paddingTop', 'paddingBottom', 'paddingLeft', 'paddingRight', 'borderWidth', 'borderRadius',
                            'marginTop', 'marginBottom', 'marginLeft', 'marginRight', 'fontSize', 'lineHeight', 'fontWeight'];
    let value =  getComputedStyle(DOM)[property];
    if(numberProperty.indexOf(property) > -1 ) {
        if(value.includes('px')) {
            let length = value.length;
            return Number(Number(value.slice(0, length-2)).toFixed(2));
        }
        return Number(value);
    }

    return value;
}

export default getCSSProperty;