const renderTags = (listTags) => {
    if(listTags.length == 0) {
        return;
    }
    return listTags.map(tag => {
            return <tag.type key={tag.id} id={tag.id} name={tag.name} className={tag.className} style={tag.style} >
                        {tag.text}
                        {renderTags(tag.children)}
                    </tag.type>                   
        })
}

export default renderTags;