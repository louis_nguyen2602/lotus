const hexDigits = new Array
        ("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"); 


const hex = (x) => {
  return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
}

//Function to convert rgb color to hex format
export const rgb2hex = (rgb) => {
        if(!rgb) {
                return;
        }
        rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
        return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
       }

export const colorToObject = (color) => {
        if(typeof color !== 'string') {
                return color;
        }
        if(color.includes('rgb')) {
                let colorArray = color.replace('rgb(','').replace(')','').split(',');
                return ({
                        r: colorArray[0],
                        g: colorArray[1],
                        b: colorArray[2],
                        a: '1'
                })
        }
        let colorArray = color.replace('rgba(','').replace(')','').split(',');
        return ({
                r: colorArray[0],
                g: colorArray[1],
                b: colorArray[2],
                a: colorArray[3]
        })
}