const modifiStyleInstandly = (listTags, id, prop) => {
    if(!listTags) {
        return new Array();
    }
     if(listTags.findIndex(tag => tag.id === id) > -1) {
        return listTags.map(tag => {
            if(tag.id === id) {
                return {
                        ...tag, 
                        style: {...tag.style, ...prop}
                        }
            } else {
                return {...tag};
            }
        })
    } else {
        return listTags.map(tag => {
            return {...tag, children: modifiStyleInstandly(tag.children, id, prop)};
        })
    }
}

export default modifiStyleInstandly;