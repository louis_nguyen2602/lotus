const findTagById = (listTags, id) => {
    if(!listTags) {
        return;
    }
    
    if(listTags.findIndex(tag => tag.id === id) > -1) {
        return listTags.find(tag => tag.id === id);
    } else {
        let result;
        for (let i = 0; i < listTags.length; i++) {
            if(findTagById(listTags[i].children, id) !== undefined) {
                result = findTagById(listTags[i].children, id);
                break;
            }
        }

        return result;
    }
}

export default findTagById;