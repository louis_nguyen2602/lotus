const emptyTag = (listTags, id) => {
        if(listTags.findIndex(tag => tag.id === id) > -1) {
            return listTags.map(tag => {
                if(tag.id === id ) {
                    return {...tag, children: new Array()};
                } else {
                    return {...tag};
                }
            })
        } else {
            return listTags.map(tag => {
                return {...tag, children: emptyTag(tag.children)};
            })
        }
    }

export default emptyTag;