const deleteTag = (listTags, id) => {
    if(listTags.findIndex(tag => tag.id === id) > -1) {
         return listTags.filter(tag => {
             return tag.id !== id;
         });
    } else {
        return listTags.map(tag => {
            return {...tag, children: deleteTag(tag.children, id)}
        })
    }
}

export default deleteTag;