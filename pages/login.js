import {useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { uploadNormal } from '../helpers/Network';
import { API_SEND_SMS, API_SEND_EMAIL_OTP, API_CONFIRM_SMS_OTP, API_RESEND_SMS_OTP, API_CONFIRM_EMAIL_OTP } from '../APIConfig';
import { setCookie, removeCookie } from '../helpers/cookies';
import { nextStepAuth } from '../actions/authAction';
import Router from 'next/router';


const login = () => {
    const [emailMobile, setEmailMobile] = useState('');
    const [typeValueInput, setTypeValueInput] = useState('');
    const [OTP, setOTP ] = useState(undefined);
    const [errMsg, setErrMsg ] = useState('');
    const dispatch = useDispatch();
    const auth = useSelector((state)=> state.auth);
    console.log("redux store:", auth);


    const handleEmailMobileInput = (ev) => {
        setEmailMobile(ev.target.value);
    }

    const handleOTPInput = (ev) => {
        setOTP(ev.target.value);
    }

    const handleKeyPress = (ev) => {
        let keyCode = ev.which || ev.keyCode;
        if (keyCode < 47 || keyCode > 58 ) {
            ev.preventDefault();
        }
    }

    const handleRequireOTP = () => {
        // validate format email or phone and call api
        if(emailMobile.trim() === '') {
            setErrMsg('Vui lòng nhập số điện thoại hoặc email');
            return;
        }
        if(isNaN(parseInt(emailMobile.trim()))) {
            const regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if(regexEmail.test(emailMobile)) {
                setTypeValueInput("email");
                let email = new FormData();
                email.append(typeValueInput, emailMobile);
                dispatch(sendEmailMobile(API_SEND_EMAIL_OTP, email));
            } else {
                setErrMsg("Email không đúng định dạng");
                return;
            }
        } else {
            const regexPhone = /(0)+([0-9]{9}$)/;
            if(regexPhone.test(emailMobile)) {
                setTypeValueInput("mobile");
                let mobile = new FormData();
                mobile.append(typeValueInput, emailMobile);
                dispatch(sendEmailMobile(API_SEND_SMS, mobile));
            } else {
                setErrMsg("Số điện thoại không đúng định dạng");
                return;
            }
        }
    } 

    const sendEmailMobile = (endpoint,data) => {
        return async dispatch => {
            try {
                const res =  await uploadNormal(endpoint, data, 'POST');
                if(!res.success) {
                    setErrMsg(res.message);
                    return;
                }
                setErrMsg('');
                let expires = Math.floor(res.data.expires_in/3600);
                setCookie("access_token_temp", res.data.access_token, expires);
                dispatch(nextStepAuth(false,'input-otp'));
            } catch(err) {
                setErrMsg(err.message);
            } 
        }
    }

    const handleLogin = () => {
        // if (!(OTP !== undefined && OTP.length >= 6)) {
        //     return;
        // }
        let OTPData = new FormData();
        OTPData.append(typeValueInput, emailMobile);
        OTPData.append('OTP', OTP);
        dispatch(sendOTP(OTPData));
    }

    const sendOTP = (data) => {
        return async dispatch => {
            try {
                let endpoint = (typeValueInput === "mobile") ? API_CONFIRM_SMS_OTP : API_CONFIRM_EMAIL_OTP;
                let res = await uploadNormal(endpoint, data, 'POST');
                if(!res.success) {
                    setErrMsg(res.message);
                    return;
                }
                //update token
                removeCookie('access_token_temp');
                let expires = Math.floor(res.data.expires_in/3600);
                setCookie("access_token", res.data.access_token, expires);
                dispatch(nextStepAuth(true,'input-mobile-email'));
                Router.push('/');
            } catch(err) {
                setErrMsg(err.message);
            }
        }
    }

    const handleResendOTP = () => {
        if(typeValueInput === "email") {
            return;
        }
        let mobile = new FormData();
        mobile.append(typeValueInput, emailMobile);
        dispatch(resendOTP(mobile));
    }

    const resendOTP = data => {
        return async dispatch => {
            try {
                let res = await uploadNormal(API_RESEND_SMS_OTP, data, 'POST');
                if(!res.success) {
                    setErrMsg(res.message);
                    return;
                }
                return;
            } catch(err) {
                setErrMsg(err.message);
            }
        }
    }

    const handleLoginAgain = () => {
        dispatch(nextStepAuth(false, 'input-mobile-email'));
        Router.push('/login');
    }

    return (
       <div className="ims-login-form">
          <div className="register-form dialog-form">
             {
                auth.step === "input-mobile-email" && <>
                    <p className="title">Đăng nhập</p>
                    <div className="input-info-wrapper">
                        <input className="input-info" placeholder="Nhập số điện thoại hoặc email"  type="text" onChange={handleEmailMobileInput} />
                        {errMsg !== '' && <div className="error-message">{`${errMsg}`}</div>}
                    </div>
                    <button className="next-btn" onClick={handleRequireOTP}>Đăng nhập</button>
                </>
             }

             {
                 auth.step === "input-otp" && <>
                    <div className="logo-confirm-wrapper">
                        <img src="/images/keylocks.svg" alt="key" className="logo-confirm" />
                    </div>
                    <p className="title">Nhập mã xác nhận OTP</p>
                    <p className="notice-message">Hãy nhập mã xác nhận đang hiển thị trên ứng dụng Lotus</p>
                    <div className="input-info-wrapper">
                        <input className="input-info" type="text" onChange={handleOTPInput} onKeyPress={handleKeyPress} />
                        {errMsg !== '' && <div className="error-message">{`${errMsg}`}</div>}
                    </div>
                    <button className={(OTP !== undefined && OTP.length >= 6) ? "next-btn": "next-btn disabled"} disabled={!(OTP !== undefined && OTP.length >= 6)} onClick={handleLogin}>
                        Tiếp tục
                    </button>
                    <p className="notice">
                        Nếu Không nhận được mã OTP từ ứng dụng, mời bạn
                        <span onClick={handleResendOTP}> lấy mã SMS OTP</span>  qua số điện thoại <br/> hoặc <br/>
                    </p>
                    <p className="notice">
                        <span onClick={handleLoginAgain}>Sử dụng tài khoản khác</span>
                    </p>
                 </>
             }
             
            <p className="notice">Tải app để đăng ký tài khoản</p>
            <p className="download-apps">
                <a className="btn-download" href="https://apps.apple.com/us/app/lotus-m%E1%BA%A1ng-x%C3%A3-h%E1%BB%99i/id1469015329?ls=1" target="_blank" rel="nofollow">
                    <img src="/images/apple.svg" alt="apple store" className="icon icon-apple"/>
                    <b>App Store</b>
                </a>
                <a className="btn-download" href="https://play.google.com/store/apps/details?id=com.vivavietnam.lotus" target="_blank" rel="nofollow">
                    <img src="/images/google-store.svg" alt="google store" className="icon play-store"/>
                    <b>Google Play</b>
                </a>
           </p>
         </div>
           
           
         
           <style jsx>{`
                .ims-login-form {
                    width: 480px;
                    margin-left: auto;
                    margin-right: auto;
                    font-family: Arial,Helvetica,sans-serif;
                    font-size: 14px;
                    line-height: 142%;
                }
                }
                .ims-login-form .dialog-form {
                    flex-direction: column;
                    padding: 65px 88px 50px;
                    z-index: 1;
                    background-color: #fff;
                    box-sizing: border-box;
                }
                .ims-login-form .dialog-form, .ims-login-form .upload-btn {
                    width: 100%;
                    border-radius: 4px;
                    display: flex;
                    align-items: center;
                }
                .ims-login-form .title {
                    font-size: 24px;
                    line-height: 28px;
                    color: #333;
                    margin-bottom: 40px;
                    text-align: center;
                }
                .ims-login-form .input-info-wrapper {
                    width: 100%;
                    margin-bottom: 15px;
                }
                .input-info-wrapper .input-info {
                    width: 100%;
                    border-radius: 4px;
                    padding: 16px 24px;
                    font-size: 14px;
                    margin-bottom: 5px;
                    letter-spacing: .75px;
                    opacity: .72;
                    border: 1px solid #d1d1d1;
                    box-sizing: border-box;
                }
                .next-btn {
                    width: 100%;
                    height: 48px;
                    background-color: #f25722;
                    border: 0;
                    color: #fff;
                    font-size: 16px;
                    border-radius: 4px;
                    margin-bottom: 40px;
                    cursor: pointer;
                }
                .ims-login-form .notice {
                    font-size: 14px;
                    color: #a6a6a6;
                    text-align: center;
                }
                .ims-login-form .note, .ims-login-form .notice, .ims-login-form .title {
                    margin-bottom: 15px;
                }
                .ims-login-form p {
                    margin: 0;
                }
                .ims-login-form .download-apps {
                    width: 100%;
                    display: flex;
                    align-items: center;
                    justify-content: space-between;
                }
                a {
                    text-decoration: none;
                }
                .ims-login-form .btn-download {
                    color: #555;
                    display: flex;
                    align-items: center;
                    padding: 10px;
                    border-radius: 5px;
                    border: 1px solid #eee;
                    width: 40%;
                }
                .ims-login-form .btn-download .icon {
                    margin-right: 5px;
                    width: 16px;
                    height: 18px;
                    fill: #888;
                    stroke: transparent;
                }
                .b {
                    font-weight: 700;
                }
                .ims-login-form .error-message {
                    margin-left: 5px;
                    font-size: 12px;
                    color: red;
                    margin-bottom: 5px;
                }
                .logo-confirm-wrapper {
                    margin-bottom: 25px;
                }
                .logo-confirm {
                    width: 80px;
                    height: 80px;
                    display: block;
                }
                .notice-message {
                    font-size: 14px;
                    line-height: 18px;
                    color: #a6a6a6;
                    text-align: left;
                }
                .notice {
                    font-size: 14px;
                    color: #a6a6a6;
                    text-align: center;
                }
                .ims-login-form .notice span {
                    color: #f25722;
                    cursor: pointer;
                }
                .ims-login-form .next-btn.disabled {
                    opacity: 0.3;
                    cursor: no-drop;
                }
            `}
         </style>
       </div>
       
    )
}

export default login;
