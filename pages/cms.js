import {useState, useEffect } from 'react';
import Head from 'next/head';
import {ArrowUpIcon, ArrowDownIcon, ArrowLeftIcon, ArrowRightIcon} from '@primer/octicons-react';
import ColorPicker  from '../components/colorPicker';
import SelectImage from '../components/SelectImage';
import { useSelector, useDispatch } from 'react-redux';
import { useRouter } from 'next/router';
import sendTags from '../actions/sendTags';
import  addTag  from '../helpers/addTag';
import deleteTag from '../helpers/deleteTag';
import emptyTag from '../helpers/emptyTag';
import modifiStyleInstandly from '../helpers/modifiStyleInsandly';
import modifiTextContent from '../helpers/modifiTextContent';
import getCSSProperty from '../helpers/getCSSProperty';
import findTagById from '../helpers/findTagById';
import { bottomResizer, rightResizer} from '../helpers/resizerTag';


const cms = () => {
    const dispatch = useDispatch();
    const router = useRouter();
    const [tags, setTags] = useState(new Array());
    const [selectedTag, setSelectedTag] = useState(null);
    const [idSection, setIdSection] = useState(0);
    const [idDivTags, setIdDivTags ] = useState(0);
    const [idButtonTags, setIdButtonTag ] = useState(0);
    const [idPtag, setIdPTag ] = useState(0);
    const [idImgTag, setIdImgTag] = useState(0);
    const [selectedDOM, setSelectedDOM] = useState(null);
    const [editStyle, setEditStyle] = useState(null);
    const [applyStyle, setApplyStyle] = useState(null);
    const [previousStyle, setPreviousStyle] = useState(null);
    const [cursorRelativePosition, setCursorRelativePosition] = useState(null);
    const [draggedDOM, setDraggedDOM] = useState(null);
    const [listDomMoved, setListDomMoved] = useState(new Array());
    const [resizingDOM, setResizingDOM] = useState(null);
    const [typeResize, setTypeResize] = useState('');
    const [startSize, setStartSize] = useState(null);
    const [pathImg, setPathImg] = useState("");
   

    useEffect(()=> {
        let width = getCSSProperty('width', selectedDOM);
        let height = getCSSProperty('height', selectedDOM);
        let marginTop = getCSSProperty('marginTop', selectedDOM);
        let marginBottom = getCSSProperty('marginBottom', selectedDOM);
        let marginLeft = getCSSProperty('marginLeft', selectedDOM);
        let marginRight = getCSSProperty('marginRight', selectedDOM);
        let paddingTop = getCSSProperty('paddingTop', selectedDOM);
        let paddingBottom = getCSSProperty('paddingBottom', selectedDOM);
        let paddingLeft = getCSSProperty('paddingLeft', selectedDOM);
        let paddingRight = getCSSProperty('paddingRight', selectedDOM);
        let borderWidth = getCSSProperty('borderWidth', selectedDOM);
        let borderRadius = getCSSProperty('borderRadius', selectedDOM);
        let borderStyle = getCSSProperty('borderStyle', selectedDOM);
        let backgroundColor = getCSSProperty('backgroundColor', selectedDOM);
        let borderColor = getCSSProperty('borderColor', selectedDOM);
        let fontSize = getCSSProperty('fontSize', selectedDOM);
        let lineHeight = getCSSProperty('lineHeight', selectedDOM) / fontSize;
        let color = getCSSProperty('color', selectedDOM);
        let textAlign = getCSSProperty('textAlign', selectedDOM);
        let fontWeight = getCSSProperty('fontWeight', selectedDOM);
        let fontStyle = getCSSProperty('fontStyle', selectedDOM);
        let textDecoration = getCSSProperty('textDecoration', selectedDOM).split(' ')[0];
        
        let deletedBottomResizer = deleteTag(tags, "bottom-resizer-outside");
        let deletedBottomAndRightResizer = deleteTag(deletedBottomResizer, "right-resizer-outside");
        if(selectedDOM) {
            let addedBottomResizer = addTag(deletedBottomAndRightResizer, selectedDOM.id, bottomResizer);
            let addedBottomAndRightResizer = addTag(addedBottomResizer, selectedDOM.id, rightResizer);
            setTags(addedBottomAndRightResizer);
        } else {
            setTags(deletedBottomAndRightResizer);
        }
        setEditStyle({
            width,
            height,
            marginTop,
            marginBottom,
            marginLeft,
            marginRight,
            paddingTop,
            paddingBottom,
            paddingLeft,
            paddingRight,
            borderWidth,
            borderRadius,
            borderStyle,
            fontSize,
            lineHeight,
        });
        setApplyStyle({
            borderColor,
            backgroundColor,
            color,
            textAlign,
            fontWeight,
            fontStyle,
            textDecoration
        });
    },[selectedDOM])

    useEffect(() => {
        applyCSS();
    }, [applyStyle])

    const setDOM = (element) => {
            if(selectedTag && element && (selectedTag.id === element.getAttribute('id'))) {
                setSelectedDOM(element);
            }
        return;
    }

    const renderTags = (listTags) => {
        if(listTags.length == 0) {
            return;
        }
        return listTags.map(tag => {
                return <tag.type key={tag.id} id={tag.id} name={tag.name} className={ checkisActive(tag) ? `${tag.className} active` : `${tag.className}`} 
                    onClick={(event)=> handleOnClick(event, tag)} style={tag.style} contentEditable={tag.contentEditable} 
                    ref={setDOM} suppressContentEditableWarning={true} tabIndex={tag.type === 'p' ? 1 : undefined} onBlur={tag.type === 'p' ? handleOnBlur : null}
                    onDragStart={handleOnDragStart} onMouseMove={handleOnMouseMove} onMouseUp={handleOnMouseUp} onMouseDown={handleOnMouseDown} >
                {tag.text}
                {renderTags(tag.children)}
             </tag.type>                   
        })
    }

    const renderListTags = (listTags) => {
        if(listTags.length == 0) {
            return;
        }
        let reSizerId = ['bottom-resizer-outside','bottom-resizer-inside','right-resizer-outside','right-resizer-inside'];
        return (
            <ul className="left-content-list">
                {
                    listTags.map(tag => {
                        if(reSizerId.indexOf(tag.id) === -1) {
                            return <li key={tag.id} name={tag.id} onClick={(event)=> selectTag(event, tag)} 
                                className={ checkisActive(tag) ? 'left-content-list-item active' : 'left-content-list-item'}>
                            <span className="left-content-list-item-name">{tag.name}</span>
                            {renderListTags(tag.children)}
                        </li>
                        }
                        return null;
                    })
                }
            </ul>
        )
    }

    const selectTag = (ev, tag) => {
        ev.stopPropagation();
        setSelectedTag(tag);
    }

    const resetSelectTag = (ev) => {
        setSelectedTag(null);
        setSelectedDOM(null);
    }

    const checkisActive = (tag) => {
        if( !selectedTag) {
            return false;
        } else if (tag.id !== selectedTag.id) {
            return false;
        } else {
            return true;
        }
    }
    
    const modifiSytle = (listTags, id) => {
        if(!listTags) {
            return new Array();
        }
         if(listTags.findIndex(tag => tag.id === id) > -1) {
            return listTags.map(tag => {
                if(tag.id === id) {
                    return {
                            ...tag, 
                            style: {...tag.style,...applyStyle}
                            }
                } else {
                    return {...tag};
                }
            })
        } else {
            return listTags.map(tag => {
                return {...tag, children: modifiSytle(tag.children, id)};
            })
        }
    }

    const handleAddSection = (event) => {
        event.preventDefault();
        setIdSection(idSection + 1);
        let newSection = {
            id: `section${idSection}`,
            name: `section${idSection}`,
            type: 'div',
            text: '',
            style: null,
            className: 'section-default',
            children: new Array()
        }
        setTags([...tags, newSection]);
    }

    const handleAddBlock = (event) => {
        event.preventDefault();
        let invalidParent = ['button', 'para', 'image'];
        if(!selectedTag) {
            setIdDivTags(idDivTags + 1);
            let newDiv = {
                id: `block${idDivTags}`,
                name: `block${idDivTags}`,
                type: 'div',
                text: '',
                style: null,
                className: 'block-default',
                children: new Array()
            }
            setTags([...tags, newDiv]);
        } else if ( invalidParent.indexOf(selectedTag.id.replace(/[0-9]/g,'')) > -1) {
            alert(`can not add block element to ${selectedTag.id.replace(/[0-9]/g,'')} element`);
            return;
        } else {
            setIdDivTags(idDivTags + 1);
            let newDiv = {
                id: `block${idDivTags}`,
                name: `block${idDivTags}`,
                type: 'div',
                text: '',
                style: null,
                className: 'block-default',
                children: new Array()
            };
            let newTags = addTag(tags, selectedTag.id, newDiv);
            setTags(newTags);
        }
    }

    const handleAddButton = (event) => {
        event.preventDefault();
        let invalidParent = ['button','para', 'image'];
        if(!selectedTag) {
            setIdButtonTag(idButtonTags + 1);
            let newButton = {
                id: `button${idButtonTags}`,
                name: `button${idButtonTags}`,
                type: 'button',
                text: 'Click me',
                style: null,
                className: 'button-default',
                children: new Array()
            }
            setTags([...tags,newButton]);
        } else if (invalidParent.indexOf(selectedTag.id.replace(/[0-9]/g,'')) > -1 ) {
            alert(`can not add button element to ${selectedTag.id.replace(/[0-9]/g,'')} element`);
            return;
        } else {
            setIdButtonTag(idButtonTags + 1);
            let newButton = {
                id: `button${idButtonTags}`,
                name: `button${idButtonTags}`,
                type: 'button',
                text: 'Click me',
                style: null,
                className: 'button-default',
                children: new Array()
            }
            let newTags = addTag(tags, selectedTag.id, newButton);
            setTags(newTags);
        }   
    }

    const handleAddPara = (event) => {
        event.preventDefault();
        let invalidParent = ['button', 'para', 'image'];
        if(!selectedTag) {
            setIdPTag(idPtag + 1);
            let newP = {
                id: `para${idPtag}`,
                name: `para${idPtag}`,
                type: 'p',
                text: 'text',
                style: null,
                contentEditable: true,
                className: 'para-default',
                children: new Array()
            }
            setTags([...tags,newP]);
        } else if ( invalidParent.indexOf(selectedTag.id.replace(/[0-9]/g,'')) > -1 ) {
            alert(`can not add para element to ${selectedTag.id.replace(/[0-9]/g,'')} element`);
            return;
        } else {
            setIdPTag(idPtag + 1);
            let newP = {
                id: `para${idPtag}`,
                name: `para${idPtag}`,
                type: 'p',
                text: 'text',
                style: null,
                contentEditable: true,
                className: 'para-default',
                children: new Array()
            }
            let newTags = addTag(tags, selectedTag.id, newP);
            setTags(newTags);
        }
    }

      const handleAddImg = () => {
        console.log("start add img");
        let invalidParent = ["button", "para", "image"];
        if (!selectedTag) {
          setIdImgTag(idImgTag + 1);
          let newImg = {
            id: `image${idImgTag}`,
            name: `image${idImgTag}`,
            type: "div",
            text: null,
            style: {
                backgroundImage: `url(${pathImg})`
            },
            contentEditable: false,
            className: "img-default",
            children: new Array(),
          };
          setPathImg('');
          setTags([...tags, newImg]);
        } else if (invalidParent.indexOf(selectedTag.id.replace(/[0-9]/g,'')) > -1) {
          alert(`can not add image element to ${selectedTag.id.replace(/[0-9]/g,'')} element`);
          return;
        } else {
          setIdImgTag(idImgTag + 1);
          let newImg = {
            id: `image${idImgTag}`,
            name: `image${idImgTag}`,
            type: "div",
            text: null,
            style: {
                backgroundImage: `url(${pathImg})`
            },
            contentEditable: false,
            className: "img-default",
            children: new Array(),
          };
          let newTags = addTag(tags, selectedTag.id, newImg);
          setPathImg('');
          setTags(newTags);
        }
      };

    const handleChangePathImg = (event) => {
        setPathImg(event.target.value);
    };

    const handleCloseSelectImage = (event) => {
        setPathImg('');
    }

    const handleDelete = (event)=> {
        event.preventDefault();
        if(!selectedTag) {
            return;
        }
        let newTags = deleteTag(tags, selectedTag.id);
        setTags(newTags);
        setSelectedTag(null);
    }

    const handleEmpty = (event) => {
        event.preventDefault();
        if(!selectedTag) {
            return;
        }
        let newTags = emptyTag(tags, selectedTag.id);
        setTags(newTags);
    }

    const handleInputClick = (event) => {
        setPreviousStyle(editStyle);
        // event.stopPropagation();
    }

    const handleInputChange = (event) => {
        setEditStyle({...editStyle, [event.target.name]: event.target.value});
    }

    const handleInputBlur = (event) => {
        let numberProperty = ['width', 'height','paddingTop', 'paddingBottom', 'paddingLeft', 'paddingRight', 'borderWidth', 'borderRadius',
                                'marginTop', 'marginBottom', 'marginLeft', 'marginRight', 'fontSize', 'lineHeight', 'fontWeight'];
        let positiveProperty = ['width', 'height','paddingTop', 'paddingBottom', 'paddingLeft', 'paddingRight', 'borderWidth', 'borderRadius',
                                'fontSize', 'lineHeight', 'fontWeight'];
        const isNumberProperty = (pro) => {
            return numberProperty.indexOf(pro) > -1;
        }
        const requiredPositive = (pro) => {
            return positiveProperty.indexOf(pro) > -1;
        }
        //validate number property
        if(isNumberProperty(event.target.name)) {
            let floatRegEx =  /[+-]?([0-9]*[.])?[0-9]+/;
            let value = Number(parseFloat(event.target.value)).toFixed(2);
            if(isNaN(value) || (requiredPositive(event.target.name) && Number(value)) < 0) {
                //invalid number property 
                setEditStyle(previousStyle);
                return;
            } else {
                // valid number property
                setApplyStyle({...editStyle, [event.target.name]: Number(value)});
            }
        } else {
            setApplyStyle({...editStyle, [event.target.name]: event.target.value});
        }
    }

    const handleChangeBGColor = (color) => {
        let colorString = `rgba(${color.rgb.r},${color.rgb.g},${color.rgb.b},${color.rgb.a})`;
        setApplyStyle({...editStyle, backgroundColor: colorString});
    }

    const handleChangeBorderColor = (color) => {
        let colorString = `rgba(${color.rgb.r},${color.rgb.g},${color.rgb.b},${color.rgb.a})`;
        setApplyStyle({...editStyle, borderColor: colorString});
    }

    const handleChangeColor = (color) => {
        let colorString = `rgba(${color.rgb.r},${color.rgb.g},${color.rgb.b},${color.rgb.a})`;
        setApplyStyle({...editStyle, color: colorString});
    }

    const handleChangeTextAlign = (align) => {
        setApplyStyle({...applyStyle, textAlign: align});
    }

    const handleToggleFontWeight = () => {
        if(applyStyle.fontWeight === '700') {
            setApplyStyle({...applyStyle, fontWeight: 'normal'});
        } else {
            setApplyStyle({...applyStyle, fontWeight: '700'});
        }
    }

    const handleToggleFontStyle = () => {
        if(applyStyle.fontStyle !== 'italic') {
            setApplyStyle({...applyStyle, fontStyle: 'italic'});
        } else {
            setApplyStyle({...applyStyle, fontStyle: 'normal'});
        }
    }

    const handleToggleTextDecoration = () => {
        if(applyStyle.textDecoration !== 'underline') {
            setApplyStyle({...applyStyle, textDecoration: 'underline'});
        } else {
            setApplyStyle({...applyStyle, textDecoration: 'none'});
        }
    }

    const applyCSS = () => {
        if(!selectedTag) {
            return;
        }
            let newTags = modifiSytle(tags, selectedTag.id);
            setTags(newTags);
    }

    const handleOnBlur = (event) => {
        event.stopPropagation();
        if(selectedTag && selectedTag.type === 'p') {
            let newTextcontent = selectedDOM.textContent;
            let newTags = modifiTextContent(tags, selectedTag.id, newTextcontent);
            setTags(newTags);
        }
    }

    const mainContentOnClick = (ev) => {
        console.log("main listener mouse click on: ", ev.target.id);
        resetSelectTag();
    }

    const mainContentOnMove = (event) => {
        if(resizingDOM) {
            //resizing
            let reSizerId = ['bottom-resizer-outside','bottom-resizer-inside','right-resizer-outside','right-resizer-inside'];
            event.target.style.cursor = (typeResize === 'height') ? 'ns-resize' : 'ew-resize';
            if(reSizerId.indexOf(event.target.id) === -1 && listDomMoved.indexOf(event.target.id) === -1) {
                setListDomMoved([...listDomMoved, event.target.id]);
            }
            if(typeResize === 'height') {
                let newHeight = event.clientY - cursorRelativePosition.clientY + startSize.height;
                let newTags = modifiStyleInstandly(tags, resizingDOM.id, {height: newHeight});
                setEditStyle({...editStyle, height: newHeight});
                setTags(newTags);
            }
            if(typeResize === 'width') {
                let newWidth = event.clientX - cursorRelativePosition.clientX + startSize.width;
                let newTags = modifiStyleInstandly(tags, resizingDOM.id, {width: newWidth});
                setEditStyle({...editStyle, width: newWidth});
                setTags(newTags);
            }
        }
    }

    const mainContentOnMouseUp = (event) => {
        if(draggedDOM) {
            if(listDomMoved.length > 0) {
                for(let i=0; i < listDomMoved.length; i++ ) {
                    document.getElementById(listDomMoved[i]).style.cursor = 'auto';
                }
            }
            setListDomMoved(new Array());
            setDraggedDOM(null);
        }
        if(resizingDOM) {
            if(listDomMoved.length > 0) {
                for(let i=0; i < listDomMoved.length; i++ ) {
                    document.getElementById(listDomMoved[i]).style.cursor = 'auto';
                }
            }
            setListDomMoved(new Array());
            setResizingDOM(null);
        }
        return; 
    }

    const leftContentOnClick = (event) => {
        resetSelectTag();
    }

    const handleOnClick = (ev, tag) => {
            ev.stopPropagation();
            console.log("mouse click on ", ev.target.id);
            let reSizerId = ['bottom-resizer-outside','bottom-resizer-inside','right-resizer-outside','right-resizer-inside'];
            if(reSizerId.indexOf(ev.target.id) > -1) return;
            selectTag(ev, tag);
    }

    const handleOnMouseDown = (event) => {
        let reSizerId = ['bottom-resizer-outside','bottom-resizer-inside','right-resizer-outside','right-resizer-inside'];
        if(reSizerId.indexOf(event.target.id) > -1) {
            //resizing
            let resizeHeight = ['bottom-resizer-outside','bottom-resizer-inside'];
            if(resizeHeight.indexOf(event.target.id) > -1) {
                //resizing height
                setTypeResize('height');
            } else {
                //resizing width
                setTypeResize('width');
            }
            setResizingDOM(selectedDOM);
            setDraggedDOM(null);
            let width = getCSSProperty('width', selectedDOM);
            let height = getCSSProperty('height', selectedDOM);
            setStartSize({width, height});
            setCursorRelativePosition({...cursorRelativePosition, clientX: event.clientX, clientY: event.clientY});
        } else {
            //dragging
            setResizingDOM(null);
            if(event.target.id.replace(/[0-9]/g,'') === 'section') return;
            setDraggedDOM(event.target);
            let shiftX = event.clientX - event.target.getBoundingClientRect().left;
            let shiftY = event.clientY - event.target.getBoundingClientRect().top;
            setCursorRelativePosition({...cursorRelativePosition, shiftX, shiftY});
        }
        
    }

    const handleOnMouseMove = (event) => {
        event.stopPropagation();
        if(draggedDOM) {
            //dragging
            event.target.style.cursor = 'move';
            if(listDomMoved.indexOf(event.target.id) === -1) {
                setListDomMoved([...listDomMoved, event.target.id]);
            }
            let draggedNode = document.getElementById(draggedDOM.id);
            let parentNode = draggedNode.parentNode;
            let newTop = (event.clientY - parentNode.getBoundingClientRect().top - cursorRelativePosition.shiftY) + 'px';
            let newLeft = (event.clientX - parentNode.getBoundingClientRect().left - cursorRelativePosition.shiftX) + 'px';
            let newTags = modifiStyleInstandly(tags, draggedDOM.id, {top: newTop, left: newLeft});
            setTags(newTags);
        } 
        if(resizingDOM) {
            //resizing
            let reSizerId = ['bottom-resizer-outside','bottom-resizer-inside','right-resizer-outside','right-resizer-inside'];
            event.target.style.cursor = (typeResize === 'height') ? 'ns-resize' : 'ew-resize';
            if(reSizerId.indexOf(event.target.id) === -1 && listDomMoved.indexOf(event.target.id) === -1) {
                setListDomMoved([...listDomMoved, event.target.id]);
            }
            if(typeResize === 'height') {
                let newHeight = event.clientY - cursorRelativePosition.clientY + startSize.height;
                let newTags = modifiStyleInstandly(tags, resizingDOM.id, {height: newHeight});
                setEditStyle({...editStyle, height: newHeight});
                setTags(newTags);
            }
            if(typeResize === 'width') {
                let newWidth = event.clientX - cursorRelativePosition.clientX + startSize.width;
                let newTags = modifiStyleInstandly(tags, resizingDOM.id, {width: newWidth});
                setEditStyle({...editStyle, width: newWidth});
                setTags(newTags);
            }
        }
    }

    const handleOnMouseUp = (event) => {
        console.log("mouse up on ", event.target.id);
        event.stopPropagation();
        if(draggedDOM) {
            //dragging
            let parentAndChild = {
                section: {
                    section: false,
                    block: true,
                    para: true,
                    button: true,
                    image: true
                },
                block: {
                    section: false,
                    block: true,
                    para: true,
                    button: true,
                    image: true
                },
                para: {
                    section: false,
                    block: false,
                    para: false,
                    button: false,
                    image: false
                },
                button: {
                    section: false,
                    block: false,
                    para: false,
                    button: false,
                    image: false
                },
                image: {
                    section: false,
                    block: false,
                    para: false,
                    button: false,
                    image: false
                }
            };
            let listValidDropType = ['section','block','para','button', 'image'];
            let belowNode = document.elementFromPoint(event.clientX, event.clientY);
            let draggedNode = document.getElementById(draggedDOM.id);
            if(belowNode.id === draggedNode.id) {
                event.target.hidden = true;
                let belowNode = document.elementFromPoint(event.clientX, event.clientY);
                event.target.hidden = false;
                let parentNode = draggedNode.parentNode;
                if(parentNode.id === belowNode.id) {
                    // kéo bên trong parent
                    if(listDomMoved.length > 0) {
                        for(let i=0; i < listDomMoved.length; i++ ) {
                            document.getElementById(listDomMoved[i]).style.cursor = 'auto';
                        }
                    }
                    setListDomMoved(new Array());
                    setDraggedDOM(null);
                    return;
                } else {
                    // kéo từ ngoài vào trong, drop item khác parent item
                    let dropId = belowNode.id;
                    let draggedId = draggedNode.id;
                    let draggedNodeType = draggedId.replace(/[0-9]/g,'');
                    let dropNodeType = dropId.replace(/[0-9]/g,'');
                    let listInvalidDrop = [];
                    if(listValidDropType.indexOf(dropNodeType) === -1) {
                        if(listDomMoved.length > 0) {
                            for(let i=0; i < listDomMoved.length; i++ ) {
                                document.getElementById(listDomMoved[i]).style.cursor = 'auto';
                            }
                        }
                        setListDomMoved(new Array());
                        setDraggedDOM(null);
                    } else {
                        while(!parentAndChild[dropNodeType][draggedNodeType]) {
                            console.log("in loop");
                            listInvalidDrop = [...listInvalidDrop, dropId];
                            for (let i = 0; i < listInvalidDrop.length; i++) {
                                document.getElementById(listInvalidDrop[i]).hidden = true;
                            }
                            belowNode = document.elementFromPoint(event.clientX, event.clientY);
                            for (let i = 0; i < listInvalidDrop.length; i++) {
                                document.getElementById(listInvalidDrop[i]).hidden = false;
                            }
                            dropId = belowNode.id;
                            dropNodeType = dropId.replace(/[0-9]/g,'');
                        }
                            let newTop = (event.clientY - belowNode.getBoundingClientRect().top - cursorRelativePosition.shiftY) + 'px';
                            let newLeft = (event.clientX - belowNode.getBoundingClientRect().left - cursorRelativePosition.shiftX) + 'px';
                            let draggedObjet = findTagById(tags, draggedId);
                            let newDraggedObject = {...draggedObjet,style:{...draggedObjet.style,top: newTop, left: newLeft }};
                            let deletedTags = deleteTag(tags, draggedId);
                            let newTags = addTag(deletedTags, dropId, newDraggedObject);
                            setTags(newTags);
                            if(listDomMoved.length > 0) {
                                for(let i=0; i < listDomMoved.length; i++ ) {
                                    document.getElementById(listDomMoved[i]).style.cursor = 'auto';
                                }
                            }
                            setListDomMoved(new Array());
                            setDraggedDOM(null);
                    }
                }
            } else {
                    // kéo từ trong ra ngoài, drop item khác parent item
                    let dropId = belowNode.id;
                    let draggedId = draggedNode.id;
                    let dropNodeType = dropId.replace(/[0-9]/g,'');
                    let draggedNodeType = draggedId.replace(/[0-9]/g,'');
                    let listInvalidDrop = [];
                    while(!parentAndChild[dropNodeType][draggedNodeType]) {
                        listInvalidDrop = [...listInvalidDrop, dropId];
                        for(let i = 0; i < listInvalidDrop.length; i++ ) {
                            document.getElementById(listInvalidDrop[i]).hidden = true;
                        }
                        belowNode = document.elementFromPoint(event.clientX, event.clientY);
                        for(let i = 0; i < listInvalidDrop.length; i++) {
                            document.getElementById(listInvalidDrop[i]).hidden = false;
                        }
                        dropId = belowNode.id;
                        dropNodeType = dropId.replace(/[0-9]/g,'');
                    }
                    let newTop = (event.clientY - belowNode.getBoundingClientRect().top - cursorRelativePosition.shiftY) + 'px';
                    let newLeft = (event.clientX - belowNode.getBoundingClientRect().left - cursorRelativePosition.shiftX) + 'px';
                    let draggedObjet = findTagById(tags, draggedId);
                    let newDraggedObject = {...draggedObjet,style:{...draggedObjet.style,top: newTop, left: newLeft }};
                    console.log("newDragged ", newDraggedObject);
                    let deletedTags = deleteTag(tags, draggedId);
                    let newTags = addTag(deletedTags, dropId, newDraggedObject);
                    setTags(newTags);
                    if(listDomMoved.length > 0) {
                        for(let i=0; i < listDomMoved.length; i++ ) {
                            document.getElementById(listDomMoved[i]).style.cursor = 'auto';
                        }
                    }
                    setListDomMoved(new Array());
                    setDraggedDOM(null);      
            }
        }

        if(resizingDOM) {
            //resizing
            if(listDomMoved.length > 0) {
                for(let i=0; i < listDomMoved.length; i++ ) {
                    document.getElementById(listDomMoved[i]).style.cursor = 'auto';
                }
            }
            setListDomMoved(new Array());
            setResizingDOM(null);
        }
    }

    const handleOnDragStart = (event) => {
        return false;
    }

    const handlePreview = (event) => {
        event.preventDefault();
        dispatch(sendTags(tags));
        router.push('/preview');
    }

    return (
        <div>
            <Head>
                <title>Piki</title>
                <link rel='shortcut icon' type='image/x-icon' href='/images/lotus-logo.png' />
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossOrigin="anonymous" />
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
            </Head>
            <SelectImage path={pathImg} handleChange={handleChangePathImg} useButtonClick={handleAddImg} handleClose={handleCloseSelectImage}/>
            <nav className="navbar navbar-expand-sm bg-dark navbar-dark">
                <a className="navbar-brand" href="#">
                    <img src="/images/lotus-logo.png" />
                </a>
                <ul className="navbar-nav">
                    <li className="nav-item" dropdown="true">
                        <a className="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="navbardrop">
                            <img src="/images/plus-icon.svg" alt="plus icon" className="menu-icon" />
                        </a>
                        <div className="dropdown-menu">
                            <a className="dropdown-item" href="#" onClick={handleAddSection}>
                                <img src="/images/section-icon.svg" alt="block-icon" className="dropdown-menu-icon"/> 
                                Section
                            </a>
                            <a className="dropdown-item" href="#" onClick={handleAddBlock}>
                                <img src="/images/rectange-icon.svg" alt="block-icon" className="dropdown-menu-icon"/> 
                               Block
                            </a>
                            <a className="dropdown-item" href="#">
                                <img src="/images/heading-icon.svg" alt="block-icon" className="dropdown-menu-icon"/>  
                                Heading
                            </a>
                            <a className="dropdown-item" href="#" onClick={handleAddPara}>
                                <img src="/images/paragraph-icon.svg" alt="paragraph-icon" className="dropdown-menu-icon"/> 
                               Paragraph  
                            </a>
                            <a className="dropdown-item" href="#" onClick={handleAddButton}>
                                <img src="/images/cursor-icon.svg" alt="button-icon" className="dropdown-menu-icon"/> 
                                Button  
                            </a>
                            <a className="dropdown-item" href="#" data-toggle="modal" data-target="#selectImg">
                                <img src="/images/picture-icon.svg" alt="picture-icon" className="dropdown-menu-icon"/> 
                                Picture  
                            </a>
                            <a className="dropdown-item" href="#">
                                <img src="/images/video-icon.svg" alt="video-icon" className="dropdown-menu-icon"/> 
                                Video  
                            </a>
                        </div>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#" onClick={handleDelete}>
                            <img src="/images/garbage-icon.svg" alt="bin icon" className="menu-icon" />   
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#" onClick={handleEmpty}>
                            <img src="/images/broom-icon.svg" alt="broom icon" className="menu-icon" />   
                        </a>
                    </li>
                </ul>
                <ul className="navbar-nav ml-auto">
                    <li className="nav-item">
                        <a className="nav-link" href="#" onClick={handlePreview}>
                            Preview   
                        </a>
                    </li>
                </ul>
            </nav>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-2 left-content"  onClick={leftContentOnClick}>
                        {renderListTags(tags)}
                    </div>
                    <div id="main-content" className="col-8 main-content" onClick={mainContentOnClick} onMouseUp={mainContentOnMouseUp} 
                        onMouseMove={mainContentOnMove} onDragStart={handleOnDragStart}>
                        {renderTags(tags)}
                    </div>
                    <div className="col-2 right-content">
                        {selectedTag && <>
                            <div className="container pt-3">
                            <div className="row right-content-row">
                                <div className="col-12 p-1">
                                    <h6 className="mb-0">Size</h6>
                                </div>
                                <div className="col-6 right-content-first-col">
                                  <label>W </label>
                                  <input value={editStyle.width} name='width' onClick={handleInputClick} onBlur={handleInputBlur} onChange={handleInputChange} />
                                </div>
                                <div className="col-6 right-content-second-col">
                                  <label>H </label>
                                  <input value={editStyle.height} name="height" onClick={handleInputClick} onBlur={handleInputBlur} onChange={handleInputChange} />
                                </div>
                            </div>
                            <div className="row right-content-row pt-2">
                                <div className="col-12 p-1">
                                    <h6 className="mb-0">Margin</h6>
                                </div>
                                <div className="col-6 right-content-first-col">
                                  <label><ArrowUpIcon size={16} /></label>
                                  <input value={editStyle.marginTop} name='marginTop' onClick={handleInputClick} onChange={handleInputChange} onBlur={handleInputBlur} />
                                </div>
                                <div className="col-6 right-content-second-col">
                                  <label><ArrowDownIcon size={16} /> </label>
                                  <input value={editStyle.marginBottom} name='marginBottom' onClick={handleInputClick} onChange={handleInputChange} onBlur={handleInputBlur} />
                                </div>
                                <div className="col-6 right-content-first-col">
                                  <label><ArrowLeftIcon size={16} /></label>
                                  <input value={editStyle.marginLeft} name='marginLeft' onClick={handleInputClick} onChange={handleInputChange} onBlur={handleInputBlur} />
                                </div>
                                <div className="col-6 right-content-second-col">
                                  <label><ArrowRightIcon size={16} /></label>
                                  <input value={editStyle.marginRight} name='marginRight' onClick={handleInputClick} onChange={handleInputChange} onBlur={handleInputBlur} />
                                </div>
                            </div>
                            <div className="row right-content-row pt-2">
                                <div className="col-12 p-1">
                                    <h6 className="mb-0">Padding</h6>
                                </div>
                                <div className="col-6 right-content-first-col">
                                  <label><ArrowUpIcon size={16} /></label>
                                  <input value={editStyle.paddingTop} name="paddingTop" onClick={handleInputClick} onChange={handleInputChange} onBlur={handleInputBlur} />
                                </div>
                                <div className="col-6 right-content-second-col">
                                  <label><ArrowDownIcon size={16} /> </label>
                                  <input value={editStyle.paddingBottom} name='paddingBottom' onClick={handleInputClick} onChange={handleInputChange} onBlur={handleInputBlur} />
                                </div>
                                <div className="col-6 right-content-first-col">
                                  <label><ArrowLeftIcon size={16} /></label>
                                  <input value={editStyle.paddingLeft} name='paddingLeft' onClick={handleInputClick} onChange={handleInputChange} onBlur={handleInputBlur} />
                                </div>
                                <div className="col-6 right-content-second-col">
                                  <label><ArrowRightIcon size={16} /></label>
                                  <input value={editStyle.paddingRight} name='paddingRight' onClick={handleInputClick} onChange={handleInputChange} onBlur={handleInputBlur} />
                                </div>
                            </div>
                            <div className="row right-content-row pt-2">
                                <div className="col-12 p-1">
                                    <h6 className="mb-0">Border</h6>
                                </div>
                                <div className="col-6 right-content-first-col">
                                  <label className="mr-auto">style</label>
                                  <select value={editStyle.borderStyle || ''} name='borderStyle' onClick={handleInputClick} onChange={handleInputChange} onBlur={handleInputBlur}>
                                      <option value='solid'>solid</option>
                                      <option value='dotted'>dotted</option>
                                      <option value='dashed'>dashed</option>
                                      <option value='double'>double</option>
                                      <option value='hidden'>hidden</option>
                                      <option value='inherit'>inherit</option>
                                      <option value='initial'>initial</option>
                                      <option value='none'>none</option>
                                  </select>
                                </div>
                                <div className="col-6 right-content-second-col">
                                  <label>width</label>
                                  <input value={editStyle.borderWidth} name='borderWidth' onClick={handleInputClick} onChange={handleInputChange} onBlur={handleInputBlur} />
                                </div>
                                <div className="col-12 right-content-second-col">
                                  <label>radius</label>
                                  <input value={editStyle.borderRadius} name='borderRadius' onClick={handleInputClick} onChange={handleInputChange} onBlur={handleInputBlur} />
                                </div>
                                <div className="col-12 right-content-first-col mt-1">
                                  <ColorPicker color={applyStyle.borderColor} handleOnChange={handleChangeBorderColor} />
                                  <input value={applyStyle.borderColor} className="ml-2"/>
                                </div>
                            </div>
                            <div className="row right-content-row pt-2">
                                <div className="col-12 p-1">
                                    <h6 className="mb-1">Background-color</h6>
                                </div>
                                <div className="col-12 right-content-first-col">
                                  <ColorPicker color={applyStyle.backgroundColor} handleOnChange={handleChangeBGColor} />
                                  <input value={applyStyle.backgroundColor} className="ml-2" />
                                </div>
                            </div>
                       </div>
                       </>
                        }
                        {
                            selectedTag && (selectedTag.type === 'p') && <>
                                <div className="container">
                                    <div className="row pt-2 right-content-row">
                                        <div className="col-3 pl-1">
                                            {
                                               applyStyle.textAlign === 'left' ? <img src="/images/text-left-active.svg" className="text-align" alt="text-left-active"/> :
                                                <img src="/images/text-left.svg" alt="text-left" className="text-align" name="left" onClick={() => handleChangeTextAlign('left')}/>
                                            }
                                            
                                        </div>
                                        <div className="col-3 pl-1">
                                            {
                                               applyStyle.textAlign === 'center' ? <img src="/images/text-center-active.svg" className="text-align" alt="text-center-active"/> :
                                               <img src="/images/text-center.svg" alt="text-center" className="text-align" name="center" onClick={() => handleChangeTextAlign('center')} /> 
                                            }
                                        </div>
                                        <div className="col-3 pl-1">
                                            {
                                               applyStyle.textAlign === 'right' ? <img src="/images/text-right-active.svg" className="text-align" alt="text-right-active"/> :
                                               <img src="/images/text-right.svg" alt="text-center" className="text-align" name="right" onClick={() => handleChangeTextAlign('right')} /> 
                                            }
                                        </div>
                                        <div className="col-3 pl-1">
                                            {
                                               applyStyle.textAlign === 'justify' ? <img src="/images/text-justify-active.svg" className="text-align" alt="text-justify-active"/> :
                                               <img src="/images/text-justify.svg" alt="text-justify" className="text-align" name="justify" onClick={() => handleChangeTextAlign('justify')} /> 
                                            }
                                        </div>
                                    </div>
                                    <div className="row pt-2 right-content-row">
                                        <div className="col-4 pl-1">
                                            {
                                                applyStyle.fontWeight === '700' ? <img src="/images/font-bold-active.svg" className="font-bold" alt="font-bold-active" onClick={handleToggleFontWeight}/> :
                                                <img src="/images/font-bold.svg" alt="font-bold" className="font-bold" onClick={handleToggleFontWeight} />
                                            }
                                            
                                        </div>
                                        <div className="col-4 pl-1">
                                            {
                                                applyStyle.fontStyle === 'italic' ? <img src="/images/font-italic-active.svg" alt="font-italic-active" className="font-italic" onClick={handleToggleFontStyle} /> :
                                                <img src="/images/font-italic.svg" alt="font-italic" className="font-italic" onClick={handleToggleFontStyle} />         
                                            }
                                        </div>
                                        <div className="col-4 pl-1">
                                            {
                                                applyStyle.textDecoration === 'underline' ? <img src="/images/font-underline-active.svg" alt="font-underline-active" className="font-underline" onClick={handleToggleTextDecoration} /> :
                                                <img src="/images/font-underline.svg" alt="font-underline" className="font-underline" onClick={handleToggleTextDecoration} />
                                            }
                                        </div>
                                    </div>
                                    <div className="row pt-2 right-content-row">
                                        <div className="col-6 right-content-first-col">
                                            <label className="pl-1"><img src="/images/font-size.svg" alt="font-size" /></label>
                                            <input value={editStyle.fontSize} name='fontSize' onClick={handleInputClick} onBlur={handleInputBlur} onChange={handleInputChange} />
                                        </div>
                                        <div className="col-6 right-content-first-col">
                                            <label><img src="images/line-height.svg" /></label>
                                            <input value={editStyle.lineHeight} name='lineHeight' onClick={handleInputClick} onBlur={handleInputBlur} onChange={handleInputChange} />
                                        </div>
                                    </div>
                                    <div className="row right-content-row pt-2">
                                        <div className="col-12 p-1">
                                            <h6 className="mb-1">Font-color</h6>
                                        </div>
                                        <div className="col-12 right-content-first-col">
                                            <ColorPicker color={applyStyle.color} handleOnChange={handleChangeColor} />
                                            <input value={applyStyle.color} className="ml-2" />
                                        </div>
                                    </div>
                                </div>
                            </>
                        }
                    </div>
                </div>
            </div>
            <style>{`
                .navbar-nav .nav-item {
                    margin-left: 10px;
                }
                .menu-icon {
                    width: 25px;
                    height: 25px;
                    margin-top: 7.5px;
                    cursor: pointer;
                }
                .dropdown-toggle::after {
                    border: none;
                    margin-left: 0px;
                }
                .dropdown-item:not(:last-child) {
                    border-bottom: 1px solid rgba(6,21,40,.03);
                }
                .dropdown-menu-icon {
                    vertical-align: baseline;
                    margin-right: 10px;
                }
                .left-content, .right-content {
                    background-color: rgba(237, 237, 237, 0.5);
                    height: 100vh;
                    padding-left: 0px;
                    padding-right: 0px;
                }
                .main-content {
                    padding-left: 10px;
                    padding-right: 10px;
                    padding-top: 10px;
                    overflow: scroll;
                }
                ul.left-content-list {
                    padding-left: 0px;
                    padding-right: 0px;
                    list-style-type: none;
                    margin-top: 0px;
                    margin-bottom: 0px;
                }
                ul li.left-content-list-item {
                    padding-top: 5px;
                    background-color: rgba(237, 237, 237, 1);
                    text-transform:capitalize;
                    line-height: 1.5;
                }
                ul li.left-content-list-item:not(:last-child) {
                    border-bottom: 1px solid rgba(6,21,40,.03);
                }
                ul li span.left-content-list-item-name {
                    margin-left: 10px;
                }
                ul li.left-content-list-item.active {
                    background-color: rgba(187, 184, 255, 0.5);
                }
                div.active, p.active, button.active {
                    outline: #ffd1f9 solid 3px;
                }
                .block-default {
                    width: 50%;
                    height: 150px;
                    border: 1px dashed blue;
                    background-color: #fff;
                    position: absolute;
                    overflow: hidden;
                    top: 20px;
                    left: 20px;
                }
                .button-default {
                    display: inline-block;
                    margin-bottom: 0;
                    font-weight: 400;
                    text-align: center;
                    white-space: nowrap;
                    vertical-align: middle;
                    -ms-touch-action: manipulation;
                    touch-action: manipulation;
                    cursor: pointer;
                    background-image: none;
                    border: 1px solid transparent;
                    padding: 6px 12px;
                    font-size: 14px;
                    line-height: 1.42857143;
                    border-radius: 4px;
                    -webkit-user-select: none;
                    -moz-user-select: none;
                    -ms-user-select: none;
                    user-select: none;
                    color: #fff;
                    background-color: #5cb85c;
                    position: absolute;
                    top: 0px;
                    left: 0px;
                }
                .para-default {
                    background-color: transparent;
                    position: absolute;
                    width: 200px;
                }
                .section-default {
                    width: 100%;
                    height: 300px;
                    position: relative;
                    border: 1px dashed red;
                    background-color: #fff;
                }
                .img-default {
                    background-repeat: no-repeat;
                    background-position: left top;
                    background-size: cover;
                    background-attachment: scroll;
                    background-origin: content-box;
                    position: absolute;
                    margin: 0 auto;
                    width: 200px;
                    height: 150px;
                }
                .right-content-row {
                    border-bottom: 1px solid rgba(6,21,40,.03);
                    padding-bottom: 8px;
                }
                .right-content-first-col, .right-content-second-col {
                    padding-left: 4px;
                    padding-right: 0px;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                }
                .right-content-first-col label, .right-content-second-col label {
                    margin-bottom: 0px;
                    margin-right: 10px;
                    font-weight: 400;
                    color: rgba(33, 37, 41, 0.5);
                    font-size: 14px;
                }
                .right-content-first-col input, .right-content-second-col input {
                    width: 80%;
                    border: none;
                    padding: 0px;
                    background-color: transparent;
                    color: #212529;
                    font-size: 14px;
                }
                .right-content-first-col input:focus, .right-content-second-col input:focus {
                    outline: none;
                }
                .right-content-first-col select {
                    border: none;
                    outline: none;
                    background-color: transparent;
                    color: rgba(33, 37, 41, 0.5);
                    font-size: 14px;
                }
                h6 {
                    color: rgba(33, 37, 41, 0.8)
                }
                .text-align, .font-bold, .font-italic, .font-underline {
                    cursor: pointer;
                }
                .modal-image {
                    position: absolute;
                    z-index: 999999999999999;
                    top: 100px;
                    left: 100px;
                }
            `}</style>
            <style jsx global>{`
                html, body {
                    padding: 0;
                    margin: 0;
                    font-family: Proxima Nova,Helvetica Neue,Helvetica,Roboto,Arial,sans-serif;
                }
                * {
                    box-sizing: border-box;
                }
            `}</style>
        </div>
    )
}

export default cms;