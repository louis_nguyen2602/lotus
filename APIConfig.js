const endPoint = "https://studio.vivavietnam.vn/api/user";
export const API_SEND_SMS = `${endPoint}/send-sms-otp`;
export const API_SEND_EMAIL_OTP = `${endPoint}/send-email-otp`;
export const API_CONFIRM_SMS_OTP = `${endPoint}/confirm-sms-otp`;
export const API_CONFIRM_EMAIL_OTP = `${endPoint}/confirm-email-otp`;
export const API_RESEND_SMS_OTP = `${endPoint}/resend-sms-otp`;
export const API_UPLOAD_IMAGE = 'https://api.imgur.com/3/upload';

// const endPoint = "http://b11.cnnd.vn/g/api/user";
